import Particles from 'react-tsparticles'
import { loadFull } from 'tsparticles'
import configJSON from '../particlesjs-config.json'


const Particle = () => {
    const particlesInit = async (main) => {
        await loadFull(main)
    }

    const particlesLoaded = (container) => {
        // console.log(container);
    }

    return (
        <Particles id="tsparticles"
                   init={particlesInit}
                   loaded={particlesLoaded}
                   options={configJSON}/>
    )
}

export default Particle