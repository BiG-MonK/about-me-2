import styled from 'styled-components'
import {InnerLayout} from '../styles/Layouts'
import Title from './Title'
import SmallTitle from './SmallTitle'
import ResumeItem from './ResumeItem'


function Resume() {
	let businessIcon =
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 768 768">
			<path d="M448.5 223.5v-63h-129v63h129zM640.5 223.5q25.5 0 44.25 19.5t18.75 45v96q0 25.5-18.75 45t-44.25 19.5h-192v-64.5h-129v64.5h-192q-27 0-45-18.75t-18-45.75v-96q0-25.5 18.75-45t44.25-19.5h127.5v-63l64.5-64.5h127.5l64.5 64.5v63h129zM319.5 511.5h129v-31.5h223.5v127.5q0 27-18.75 45.75t-45.75 18.75h-447q-27 0-45.75-18.75t-18.75-45.75v-127.5h223.5v31.5z" />
		</svg>
	let educationIcon =
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1280 768">
			<path d="M1013.714 477.714l10.286 180.571c4.571 80.571-164 146.286-365.714 146.286s-370.286-65.714-365.714-146.286l10.286-180.571 328 103.429c9.143 2.857 18.286 4 27.429 4s18.286-1.143 27.429-4zM1316.571 292.571c0 8-5.143 14.857-12.571 17.714l-640 201.143c-2.286 0.571-4 0.571-5.714 0.571s-3.429 0-5.714-0.571l-372.571-117.714c-32.571 25.714-55.429 88.571-60 165.714 21.714 12.571 36 35.429 36 62.286 0 25.714-13.143 48-33.143 61.143l33.143 247.429c0.571 5.143-1.143 10.286-4.571 14.286s-8.571 6.286-13.714 6.286h-109.714c-5.143 0-10.286-2.286-13.714-6.286s-5.143-9.143-4.571-14.286l33.143-247.429c-20-13.143-33.143-35.429-33.143-61.143 0-27.429 15.429-50.857 37.143-63.429 3.429-66.857 20.571-138.857 56-188.571l-190.286-59.429c-7.429-2.857-12.571-9.714-12.571-17.714s5.143-14.857 12.571-17.714l640-201.143c2.286-0.571 4-0.571 5.714-0.571s3.429 0 5.714 0.571l640 201.143c7.429 2.857 12.571 9.714 12.571 17.714z" />
		</svg>

	return (
		<ResumeStyled>
			<Title title={'Resume'} span={'resume'} />
			<InnerLayout>
				<div className="small-title">
					<SmallTitle icon={businessIcon} title={'Working Experience'} />
				</div>
				<div className="resume-content">
					<ResumeItem
						year={'2015 - 2020'}
						title={'Computer Science Teacher'}
						subTitle={'Sussex University'}
						text={'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil impedit natus nostrum? Velit accusantium id quos, nihil vel quod.Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. '}
					/>
					<ResumeItem
						year={'2015 - Present'}
						title={'Full Stack Developer'}
						subTitle={'Microsoft Studios'}
						text={'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil impedit natus nostrum? Velit accusantium id quos, nihil vel quod.Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. '}
					/>
					<ResumeItem
						year={'2010 - 2017'}
						title={'User Interface Designer'}
						subTitle={'Google Inc'}
						text={'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil impedit natus nostrum? Velit accusantium id quos, nihil vel quod.Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. '}
					/>
				</div>
				<div className="small-title u-small-title-margin">
					<SmallTitle icon={educationIcon} title={'Educational Qualifications'} />
				</div>
				<div className="resume-content ">
					<ResumeItem
						year={'2011 - 2022'}
						title={'Computer Science Degree'}
						subTitle={'Sussex University'}
						text={'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil impedit natus nostrum? Velit accusantium id quos, nihil vel quod.Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. '}
					/>
					<ResumeItem
						year={'2015 - 2017'}
						title={'A - Levels'}
						subTitle={'Church Hill High'}
						text={'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil impedit natus nostrum? Velit accusantium id quos, nihil vel quod.Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. '}
					/>
					<ResumeItem
						year={'2015 - 2017'}
						title={'High School Graduation'}
						subTitle={'ABC School'}
						text={'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil impedit natus nostrum? Velit accusantium id quos, nihil vel quod.Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. '}
					/>
				</div>
			</InnerLayout>
		</ResumeStyled>
	)
}

const ResumeStyled = styled.section`
    .small-title {
        padding-bottom: 3rem;
    }
	
    .u-small-title-margin {
        margin-top: 4rem;
    }

    .resume-content {
        border-left: 2px solid var(--border-color);
    }
`


export default Resume