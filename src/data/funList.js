import fun_item_1 from '../img/fun/fun_item_1.svg'

const funList = [
	{
		id: 1,
		title: 'How To Work from Home',
		date: '01',
		month: 'April',
		image: fun_item_1,
		link: 'https://www.google.com/'
	},
	{
		id: 2,
		title: 'How To Use SEO Efficiently',
		date: '01',
		month: 'April',
		image: fun_item_1,
		link: 'https://www.google.com/'
	},
	{
		id: 3,
		title: 'How to choose a programming Language',
		date: '01',
		month: 'April',
		image: fun_item_1,
		link: 'https://www.google.com/'
	},
	{
		id: 4,
		title: 'How To Tse SEO Efficiently',
		date: '01',
		month: 'April',
		image: fun_item_1,
		link: 'https://www.google.com/'
	},
	{
		id: 5,
		title: 'How To Tse SEO Efficiently',
		date: '01',
		month: 'April',
		image: fun_item_1,
		link: 'https://www.google.com/'
	}
]

export default funList