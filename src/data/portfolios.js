import css1 from '../img/portfolio/cv_css.png'
import css2 from '../img/portfolio/ms.png'
import react1 from '../img/portfolio/react_styled.png'
import img2 from '../img/portfolio/maya-4.jpg'
import img3 from '../img/portfolio/maya-3.jpg'


const portfolios = [
	{
		id: 1,
		category: 'CSS',
		image: css1,
		link1: 'https://www.google.com',
		link2: 'https://www.google.com',
		title: 'portfolio__1__CSS',
		text: 'Created with only HTML and CSS.'
	},
	{
		id: 2,
		category: 'PHP',
		image: img3,
		link1: 'https://www.google.com',
		link2: 'https://www.google.com',
		title: 'portfolio__2__PHP',
		text: 'Number one Animation Application'
	},
	{
		id: 3,
		category: 'Javascript',
		image: img3,
		link1: 'https://www.google.com',
		link2: 'https://www.google.com',
		title: 'portfolio__3__Javascript',
		text: 'Number one Animation Application'
	},
	{
		id: 4,
		category: 'Animation',
		image: img2,
		link1: 'https://www.google.com',
		link2: 'https://www.google.com',
		title: 'portfolio__4__Animation',
		text: 'Free Animation Software'
	},
	{
		id: 5,
		category: 'Animation',
		image: img2,
		link1: 'https://www.google.com',
		link2: 'https://www.google.com',
		title: 'portfolio__5__Animation',
		text: 'Free Animation Software'
	},
	{
		id: 6,
		category: 'React JS',
		image: react1,
		link1: 'https://www.google.com',
		link2: 'https://www.google.com',
		title: 'portfolio__6__React JS',
		text: 'Created using different technologies such as Material UI, Styled-Components and more...'
	},
	{
		id: 7,
		category: 'CSS',
		image: css2,
		link1: 'https://www.google.com',
		link2: 'https://www.google.com',
		title: 'portfolio__7__CSS',
		text: 'Created using HTML and CSS'
	}
]

export default portfolios