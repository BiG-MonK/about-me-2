import styled from 'styled-components'
import {MainLayout, InnerLayout} from '../styles/Layouts'
import Title from '../components/Title'
import PrimaryButton from '../components/PrimaryButton'
import ContactItem from '../components/ContactItem'
// import PhoneIcon from '@material-ui/icons/Phone'
// import EmailIcon from '@material-ui/icons/Email'
// import LocationOnIcon from '@material-ui/icons/LocationOn'



function ContactPage() {
	const phone =
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
			<path d="M368 0h-224c-26.4 0-48 21.6-48 48v416c0 26.4 21.6 48 48 48h224c26.4 0 48-21.6 48-48v-416c0-26.4-21.6-48-48-48zM192 24h128v16h-128v-16zM256 480c-17.673 0-32-14.327-32-32s14.327-32 32-32 32 14.327 32 32-14.327 32-32 32zM384 384h-256v-320h256v320z" />
		</svg>
	const email =
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
			<path d="M464 64h-416c-26.4 0-48 21.6-48 48v320c0 26.4 21.6 48 48 48h416c26.4 0 48-21.6 48-48v-320c0-26.4-21.6-48-48-48zM199.37 275.186l-135.37 105.446v-250.821l135.37 145.375zM88.19 128h335.62l-167.81 126-167.81-126zM204.644 280.849l51.356 55.151 51.355-55.151 105.277 135.151h-313.264l105.276-135.151zM312.63 275.186l135.37-145.375v250.821l-135.37-105.446z" />
		</svg>
	const location =
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
			<path d="M319.76 448.8l112.528-51.424v-334.176l-112.528 51.408v334.192zM192.24 397.376l111.68 46.288v-334.192l-111.68-46.272v334.176zM63.696 114.608v334.192l112.688-46.272v-334.192l-112.688 46.272z" />
		</svg>

	return (
		<MainLayout>
			<Title title={'Contact'} span={'Contact'} />
			<ContactPageStyled >
				<InnerLayout className={'contact-section'}>
					<div className="left-content">
						<div className="contact-title">
							<h4>Get In Touch</h4>
						</div>
						<form  className="form">
							<div className="form-field">
								<label htmlFor="name">Enter your name*</label>
								<input type="text" id="name" />
							</div>
							<div className="form-field">
								<label htmlFor="email">Enter your email*</label>
								<input type="email" id="email" />
							</div>
							<div className="form-field">
								<label htmlFor="subject">Enter your subject</label>
								<input type="text" id="subject" />
							</div>
							<div className="form-field">
								<label htmlFor="textarea">Enter your Message*</label>
								<textarea name="textarea" id="textarea" cols="30" rows="10"></textarea>
							</div>
							<div className="form-field f-button">
								<PrimaryButton title={'Send Email'} />
							</div>
						</form>
					</div>
					<div className="right-content">
						<ContactItem title={'Phone'} icon={phone} cont1={'+66-789675637'} cont2={'07663520283'} />
						<ContactItem title={'Email'} icon={email} cont1={'loremipsum@gmail.com'} cont2={'info.lorem.ipsum@gmail.com'} />
						<ContactItem title={'Address'} icon={location} cont1={'27 Aldrich Road, London, England'} cont2={'United Kingdom'} />
					</div>
				</InnerLayout>
			</ContactPageStyled>
		</MainLayout>
	)
}

const ContactPageStyled = styled.section`
    .contact-section {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-column-gap: 2rem;
	    
        @media screen and (max-width: 978px) {
            grid-template-columns: repeat(1, 1fr);
	        
            .f-button {
                margin-bottom: 3rem;
            }
        }
	    
        .right-content {
            display: grid;
            grid-template-columns: repeat(1, 1fr);
	        
            @media screen and (max-width: 502px) {
                width: 70%;
            }
        }
	    
        .contact-title {
            h4 {
                color: var(--white-color);
                padding: 1rem 0;
                font-size: 1.8rem;
            }
        }
	    
        .form {
            width: 100%;
	        
            @media screen and (max-width: 502px) {
                width: 100%;
            }
	        
            .form-field {
                margin-top: 2rem;
                position: relative;
                width: 100%;
	            
                label {
                    position: absolute;
                    left: 20px;
                    top: -19px;
                    display: inline-block;
                    background-color: var(--background-dark-color);
                    padding:0 .5rem;
                    color: inherit;
                }
	            
                input {
                    border: 1px solid var(--border-color);
                    outline: none;
                    background: transparent;
                    height: 50px;
                    padding:0 15px;
                    width: 100%;
                    color: inherit;
                }
	            
                textarea {
                    background-color: transparent;
                    border: 1px solid var(--border-color);
                    outline: none;
                    color: inherit;
                    width: 100%;
                    padding: .8rem 1rem;
                }
            }
        }
    }
`


export default ContactPage