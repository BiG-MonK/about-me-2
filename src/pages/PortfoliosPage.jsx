import portfolios from '../data/portfolios'
import {useState} from 'react'
import {InnerLayout, MainLayout} from '../styles/Layouts'
import Title from '../components/Title'
import Button from '../components/Button'
import Menu from '../components/Menu'


const allButtons = ['All', ...new Set(portfolios.map(item => item.category))]

function PortfoliosPage() {
	const [menuItem, setMenuItems] = useState(portfolios)
	const [button, setButtons] = useState(allButtons)

	const filter = (button) => {
		if (button === 'All') {
			setMenuItems(portfolios)
			return
		}
		const filteredData = portfolios.filter(item => item.category === button)
		setMenuItems(filteredData)
	}

	return (
		<MainLayout>
			<Title title={'Portfolios'} span={'portfolios'} />
			<InnerLayout>
				<Button filter={filter} button={button} />
				<Menu menuItem={menuItem} />
			</InnerLayout>
		</MainLayout>
	)
}


export default PortfoliosPage